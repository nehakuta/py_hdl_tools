# Python HDL tools

Various python tools for working with VHDL.

## Installing/Upgrading

Open a command line terminal, make sure you can run the ``git`` command correctly. 
Run the following command:

<pre>
python -m pip install -e git+git@grumpy.omicron.at:hw/hdlparse.git#egg=hdlparse
python -m pip install -e git+git@grumpy.omicron.at:hw/py_hdl_tools.git#egg=py_hdl_tools
</pre>

Note that we need our version of `hdlparse`, not the version from the internet.
Now you should be able to open an interactive python shell and run:

<pre>import py_hdl_tools</pre>

If no error is thrown, you have a correct installation.

## Classes

- **HdlFile():** Parse VHDL files and extract provided entities, functions, 
  constants and also extract dependencies to other packages and entities.
  Extracting the constants can be used to create C header files from VHDL
  package files.
- **HdlDependency():** Automatically find all dependencies of a VHDL file by
  searching through repositories. The class fill return a file list in the 
  correct compilation order. This list than can be used to to generate a 
  Modelsim compile script or a Quartus synthesis script. Only the minimum
  number of necessary files will be returned which speeds up the compilation
  process.
- **HdlSim():** Run VHDL simulation and automatically find all dependencies.

## Simulation script

**Usage:**
<pre>
# python -m py_hdl_tools.sim_module -h
usage: sim_module.py [-h] [--module_json module.json] [--only_compile]
                     [--no_ui] [--verbose]
                     [file|directory]

Simulate VHDL modules with Modelsim

positional arguments:
  file|directory        The testbench file or directory to simulate. If a
                        directory is given all VHDL testbenches in them will
                        be added to the Modelsim project.

optional arguments:
  -h, --help            show this help message and exit
  --module_json module.json
                        Compile and run all testbenches which are referenced
                        by module.json.
  --only_compile        Only compile and don't automatically run simulation.
  --no_ui               Don't start UI of simulator. Instead run simulator in
                        command line mode.
  --verbose             Print verbose messages.
</pre>

For simulating a single testbench just write: 

<pre>
python -m py_hdl_tools.sim_module path\to\some\testbench_tb.vhd
</pre>

Important note: When simulating a whole module or multiple modules the
testbench files must end with "_tb.vhd" for this script to
recognize it as testbench. Otherwise it won't find them.

### Limitations

- Memory initialization files are currently not copied into the Modelsim
  working directory. So Modelsim won't find these files.
- '--only_compile' option is not implemented yet. So when simulating a whole
  module with multiple testbenches all the simulations will run through 
  which can be annoying.
- The script assumes that your VHDL files are organized in a folder structure
  like this:
<pre>
repository/
	repository.json
	module_a/
		module.json
		rtl/some_rtl_file.vhd
		rtl/another_rtl_file.vhd
		tb/some_tb_file_tb.vhd
		tb/second_tb_file_tb.vhd
	module_b/
		rtl/more_rtl_files.vhd
		tb/another_tb.vhd
</pre>

## Context menu entry for simulation

The (add_context_menu.py)[py_hdl_tools/add_context_menu.py] 
script adds a "Simulate with Modelsim" context menu entry to the windows explorer.
Just start the script directly or with

<pre>
python -m py_hdl_tools.add_context_menu
</pre>

Note: Currently this entry will show up for all file types because adding this
context menu entry only to VHDL files doesn't work for some reason.

## Extract and evaluate VHDL constants

TODO

## VHDL to C header converter

This tool can be used to automatically generate C header files form a VHDL 
package.

TODO: Implement this tool

## Dependency management

TODO: show how to use the HdlDependency() class
