import sys
from py_hdl_tools import HdlFile

def print_hdl_file_info(hdl_file):
    print("Required packages:")
    for p in hdl_file.required_packages:
        print("  {}.{}.{}".format(p["library"], p["name"], p["object"]))
    print()
        
    print("Required entities:")
    for e in hdl_file.required_entities:
        print("  {}".format(e["name"]))
    print()
        
    print("Provided entities:")
    for e in hdl_file.entities:
        print("  {}".format(e.name))
    print()

    print("Provided constants:")
    for c in hdl_file.constants:
        print("  {}".format(c.name))
    print()

    print("Provided procedures:")
    for f in hdl_file.procedures:
        print("  {}".format(f.name))
    print()

    print("Provided functions:")
    for f in hdl_file.functions:
        print("  {}".format(f.name))
    print()
    
def show_hdl_file():
    if len(sys.argv) != 2:
        print("Usage: show_hdl_file <filename>")
        exit(1)

    hdl_file = HdlFile(sys.argv[1])
    print_hdl_file_info(hdl_file)

if __name__ == "__main__":
    show_hdl_file()