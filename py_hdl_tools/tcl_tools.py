
def to_tcl_str(string):
    tcl_str = string.replace('\\', '\\\\')
    tcl_str = tcl_str.replace('"', '\\"')
    tcl_str = '"' + tcl_str + '"'
    return tcl_str

def fix_filename_for_tcl(fn):
    """ Escape backslash for TCL """
    fn = fn.replace("\\", "\\\\")
    return fn

def to_tcl_list(lst):
    ret = "{\n"

    for i in lst:
        if isinstance(i, str):
            ret += "\t" + to_tcl_str(i) + " \n"
        else:
            ret += "\t" + str(i) + " \n"

    ret += "}\n"
    return ret

def test_tcl_tools():
    tst_str = 'quote \" and slash \\'
    exp_str = '"quote \\" and slash \\\\"'
    print(tst_str, "=>", to_tcl_str(tst_str))
    assert to_tcl_str(tst_str) == exp_str

    lst = ["abc", "def", '"quoted string"', "some\\path\\to\\file",
           123, 456, 123.456]
    tcl_list = to_tcl_list(lst)
    print(tcl_list)

def main():
    test_tcl_tools()

if __name__ == "__main__":
    main()
