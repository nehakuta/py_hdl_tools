import glob
import sys
import os
import json
import argparse
from py_hdl_tools import HdlSim
from pprint import pprint

here = os.path.abspath(os.path.dirname(__file__))


def get_repositories(top_dir):
    """ Get all referenced repositories from the repository.json """

    repositories = [os.path.abspath(top_dir)]
    repo_fn = os.path.abspath(os.path.join(top_dir, "repository.json"))

    try:
        f = open(repo_fn, "r")
    except FileNotFoundError:
        print("  File '{}' not found, ignoring".format(repo_fn))
    else:
        new_repos = json.load(f)["repositories"]

        for repo in new_repos:
            # Note: paths are all relative to current repository.json
            repositories += [os.path.join(top_dir, repo)]

            # TODO: search recursive repositories

    return repositories


def get_module_directory(repositories, module_name):
    """
    Search for a module with the name 'module_name' in all the repositories
    and return the full path to the module directory
    """

    for repo in repositories:
        module_dir = os.path.join(repo, module_name)
        if os.path.isdir(module_dir):
            return module_dir

    return None


def get_all_testbenches_of_module(module_dir):
    tb_files = []
    tb_file_pattern = os.path.join(module_dir, "tb", "*_tb.vhd")
    for name in glob.glob(tb_file_pattern):
        tb_files += [name]

    return tb_files


def simulate_testbench_files(tb_files, repositories, with_ui=False):
    hdl_sim = HdlSim(
        tb_files, repositories, excluded_dir_names=["synth", "sim_out", "simulate"])
    hdl_sim.simulate(with_ui=with_ui)


def show_used_repos(repositories):
    print("Using following repositories in this order:")
    for repo in repositories:
        print("  {}".format(repo))
    print()


def show_found_testbenches(tb_files):
    print("Found following testbenches:")
    for tb_file in tb_files:
        print("  '{}'".format(tb_file))
    print()


def run_all_testbenches_from_modules_json(args):
    """ 
    Load list of modules to simulate from args.module_json and run all
    the found testbenches.
    """

    # Check if there is a JSON file with the modules to simulate
    try:
        module_json_fh = open(args.module_json, "r")
    except FileNotFoundError:
        print("Error: File '{}' not found!".format(args.module_json))
        exit(1)
    else:
        print("Loading list of modules to simulate from '{}'".format(args.module_json))
        modules = json.load(module_json_fh)["modules"]

    top_dir = os.path.abspath(os.path.join(
        os.path.split(args.module_json)[0], ".."))

    run_all_testbenches_from_module_list(args, modules, top_dir)


def run_all_testbenches_from_module(args):
    """ 
    Run all testbenches in the module directory which is given 
    in args.file_or_dir.
    """
    top_dir = os.path.abspath(os.path.join(args.file_or_dir, ".."))

    run_all_testbenches_from_module_list(
        args, [args.file_or_dir], top_dir)


def run_all_testbenches_from_module_list(args, modules, top_dir):
    repositories = get_repositories(top_dir)

    show_used_repos(repositories)

    tb_files = []

    for module in modules:
        print("Collecting testbenches from '{}' ...".format(module))
        module_dir = get_module_directory(repositories, module)

        if module_dir == None:
            print("  Couldn't find module '{}' in any of the repositories".format(module))
        else:
            if args.verbose:
                print("  '{}' => '{}'".format(module, module_dir))
            tb_files += get_all_testbenches_of_module(module_dir)
    print()

    show_found_testbenches(tb_files)

    if len(tb_files) == 0:
        print("Error: no testbench files found, nothing to do!")
        exit(1)

    print("Starting Modelsim ...")
    simulate_testbench_files(tb_files, repositories, not args.no_ui)
    exit(0)


def run_single_testbench_file(args):
    """ Run a single testbench file given by args.file_or_dir """

    # Note: We assume that the testbench file is the "<module_name>/<tb>" folder
    top_dir = os.path.abspath(os.path.join(args.file_or_dir, "..", "..", ".."))

    repositories = get_repositories(top_dir)

    tb_files = [os.path.abspath(args.file_or_dir)]

    show_used_repos(repositories)
    show_found_testbenches(tb_files)

    simulate_testbench_files(tb_files, repositories, not args.no_ui)
    exit(0)


def main():
    parser = argparse.ArgumentParser(
        description="Simulate VHDL modules with Modelsim")

    parser.add_argument(
        "file_or_dir", metavar="file|directory", nargs="?", default=None,
        help="The testbench file or directory to simulate. If a directory is given all "
        "VHDL testbenches in them will be added to the Modelsim project.")
    parser.add_argument(
        "--module_json", dest="module_json", metavar="module.json", default=None,
        help="Compile and run all testbenches which are referenced by module.json.")
    parser.add_argument(
        "--only_compile", dest="only_compile", action="store_true", default=False,
        help="Only compile and don't automatically run simulation.")
    parser.add_argument(
        "--no_ui", dest="no_ui", action="store_true", default=False,
        help="Don't start UI of simulator. Instead run simulator in command line mode.")
    parser.add_argument(
        "--verbose", dest="verbose", action="store_true", default=False,
        help="Print verbose messages.")

    args = parser.parse_args()

    if args.file_or_dir != None:
        if os.path.isdir(args.file_or_dir):
            run_all_testbenches_from_module(args)
        elif os.path.isfile(args.file_or_dir):
            if os.path.splitext(args.file_or_dir)[1].lower() not in (".vhd", ".vhdl", ".vho", ".v", ".sv"):
                print("  Error: File {} is not a VHDL file.".format(
                    args.file_or_dir))
                exit(1)
            run_single_testbench_file(args)
        else:
            print("  Error: '{}' is neither directory nor a file.".format(
                args.file_or_dir))
            exit(1)

    if args.module_json != None:
        run_all_testbenches_from_modules_json(args)


if __name__ == "__main__":
    main()
