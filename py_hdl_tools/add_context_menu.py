# Add a context menu entry for VHDL files to the windows explorer named "Simulate with Modelsim"
# which will simulate the given file.

import winreg
import os
import sys


def add_context_menu_command(path_to_class):
    """ path_to_class can be for example "Software\\Classes\\*" """
    key = winreg.CreateKey(
        winreg.HKEY_CURRENT_USER, path_to_class + "\\shell\\sim_module")
    winreg.SetValueEx(
        key, None, 0, winreg.REG_EXPAND_SZ, "Simulate with Modelsim")

    icon_file = os.path.abspath(os.path.join(__file__, "..", "modelsim.ico"))

    winreg.SetValueEx(
        key, "Icon", 0, winreg.REG_EXPAND_SZ, icon_file)
    key.Close()

    key = winreg.CreateKey(
        winreg.HKEY_CURRENT_USER, path_to_class + "\\shell\\sim_module\\command")
    winreg.SetValueEx(
        key, None, 0, winreg.REG_EXPAND_SZ,
        "\"{}\" \"-m\" \"py_hdl_tools.sim_module\" \"%1\"".format(sys.executable))
    key.Close()


def add_sim_module_to_context_menu():
    # TODO: set ".vhd" key instead of "*", but this doesn't seem to work
    # add_context_menu_command("Software\\Classes\\.vhd")

    add_context_menu_command("Software\\Classes\\*")
    add_context_menu_command("Software\\Classes\\Directory")


if __name__ == "__main__":
    add_sim_module_to_context_menu()
