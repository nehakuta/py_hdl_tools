import os
import difflib
import hdlparse.vhdl_parser as vhdl
import hdlparse.verilog_parser as vlog
from pprint import pprint

here = os.path.abspath(os.path.dirname(__file__))
vhdl_ex = vhdl.VhdlExtractor()
vlog_ex = vlog.VerilogExtractor()

def _package(name, library, object="all"):
    return {
        "name": name.lower(),
        "library": library.lower(),
        "object": object.lower(),
    }

def _entity(name, library="work"):
    return {"name": name.lower(), "library": library}

def get_memory_maps(vhld_objects):
    """ Return memory maps and register definitions in SVD format """
    pass

class HdlFile:
    vhdl_file_ext = (".vhd", ".vhdl", ".vho")
    vlog_file_ext = (".v", ".sv")
	
    def __init__(self, filename=None, default_library="work"):
        self.filename = filename
        self._default_library = default_library

        self._provides = {
            "package": [],
            "component": [],
            "entity": []
        }

        self._requires = {
            "package": [],
            "entity": []
        }

        _, file_ext = os.path.splitext(filename)

        if file_ext in HdlFile.vhdl_file_ext:
            self.objects = vhdl_ex.extract_objects(filename)
        elif file_ext in HdlFile.vlog_file_ext:
            self.objects = vlog_ex.extract_objects(filename)
        else:
            raise RuntimeError("Unsupported file type: '%s'" % file_ext)

        self._extract_info_from_objects(
            self.objects, library=self._default_library)

    @staticmethod
    def _contains(a_list, name, library):
        """
        Return true if dictionary d contains a requirement/provision
        with the given name and library
        """
        for item in a_list:
            if item["name"] == name and item["library"] == library:
                return True
        return False

    @staticmethod
    def _add_if_not_in_list(a_list, item):
        if not HdlFile._contains(a_list, item["name"], item["library"]):
            a_list += [item]

    def _extract_info_from_objects(self, vhdl_objects, library="work"):
        # TODO: don't add dependencies multiple times
        for o in vhdl_objects:
            if o.kind == "package":
                HdlFile._add_if_not_in_list(
                    self._provides["package"], _package(o.name, library))
            elif o.kind == "package_use":
                HdlFile._add_if_not_in_list(
                    self._requires["package"], _package(o.name, o.library, o.obj))
            elif o.kind in ("entity", "module"):
                HdlFile._add_if_not_in_list(
                    self._provides["entity"], _entity(o.name, library))
            elif o.kind == "entity_instance":
                HdlFile._add_if_not_in_list(
                    self._requires["entity"], _entity(o.entity_name, o.library))
            elif o.kind == "component":
                HdlFile._add_if_not_in_list(
                    self._provides["component"], _entity(o.name, library))

    def __repr__(self):
        return "HdlFile(filename = '{}', provides = {}, requires = {})".format(
            self.filename, self._provides, self._requires)

    def get_objects_by_kind(self, kind):
        """ Get objects by kind of object """
        if not isinstance(kind, list):
            kind = [kind]

        ret = []
        for o in self.objects:
            if o.kind in kind:
                ret += [o]

        return ret

    @property
    def constants(self):
        return self.get_objects_by_kind(["constant"])

    @property
    def functions(self):
        return self.get_objects_by_kind(["function"])

    @property
    def procedures(self):
        return self.get_objects_by_kind(["procedure"])

    @property
    def entities(self):
        return self.get_objects_by_kind(["entity", "module"])

    def get_required(self, dep_type):
        return self._requires[dep_type]

    def get_provided(self, dep_type):
        return self._provides[dep_type]

    def remove_required_package(self, library, package_name = None):
        """
        Remove all dependencies to the given library/package combination.
        Use this to get rid of "std", "ieee", etc packages which are builtin.
        """
        new = []
        for r in self._requires["package"]:
            if library == r["library"] and \
                (package_name == None or (package_name == r["name"])):
                pass
            else:
                new += [r]
        self._requires["package"] = new

    @property
    def required_packages(self):
        """ Return list of required packages """
        return self.get_required("package")

    @property
    def required_entities(self):
        """ Return list of required entities """
        return self.get_required("entity")

    @property
    def provided_packages(self):
        """ Return list of provided packages """
        return self.get_provided("package")

    @property
    def provided_entities(self):
        """ Return list of required entities """
        return self.get_provided("entity")

    #
    # Dependency and provision checking
    #
    def requires(self, dep_type, name, library="work"):
        """ Return true if file requires the given dependency """
        assert dep_type in self._requires.keys()
        return HdlFile._contains(self._requires[dep_type], name, library)

    def provides(self, dep_type, name, library="work"):
        """ Return true if this file provides the given dependency """
        assert dep_type in self._provides.keys()
        return HdlFile._contains(self._provides[dep_type], name, library)

    def requires_package(self, package_name, library="work"):
        """ Return true if this file requires the given package """
        return self.requires("package", package_name, library)

    def requires_entity(self, entity_name, library="work"):
        """ Return true if this file requires the given entity """
        return self.requires("entity", entity_name, library)

    def provides_package(self, package_name, library="work"):
        """ Return true if this file provides the given package """
        return self.provides("package", package_name, library)

    def provides_entity(self, entity_name, library="work"):
        """ Return true if this file provides the given entity """
        return self.provides("entity", entity_name, library)

def test_hdl_file():
    hdl_package=HdlFile(os.path.join(here, "..", "test", "uart_if_mem_pkg.vhd"))
    print(hdl_package)

    obj = hdl_package.objects
    const = hdl_package.constants
    proc = hdl_package.procedures
    func = hdl_package.functions
    entity = hdl_package.entities

    assert len(const) == 16
    assert len(proc) == 0
    assert len(func) == 0
    assert len(entity) == 0

    pprint(obj)

    print("VHDL package required packages:")
    pprint(hdl_package.required_packages)
    print("VHDL package required entities:")
    pprint(hdl_package.required_entities)

    assert hdl_package.requires_package("std_logic_1164", "ieee")
    assert hdl_package.requires_package("numeric_std", "ieee")
    assert hdl_package.requires_package("fifo_if_pkg", "work")
    assert hdl_package.requires_package("avalon_lib", "work")
    assert len(hdl_package.required_packages) == 4

    assert hdl_package.provides_package("uart_if_mem_pkg")
    assert len(hdl_package.provided_packages) == 1

    hdl_entity=HdlFile(os.path.join(here, "..", "test", "uart_if_mem.vhd"))
    print(hdl_entity)

    obj = hdl_entity.objects
    const = hdl_entity.constants
    proc = hdl_entity.procedures
    func = hdl_entity.functions
    entity = hdl_entity.entities

    assert len(const) == 0
    assert len(proc) == 0
    assert len(func) == 0
    assert len(entity) == 1

    pprint(obj)

    print("VHDL entity required packages:")
    pprint(hdl_entity.required_packages)
    print("VHDL entity required entities:")
    pprint(hdl_entity.required_entities)

    assert hdl_entity.requires("package", "std_logic_1164", "ieee")
    assert hdl_entity.requires("package", "numeric_std", "ieee")
    assert hdl_entity.requires("package", "avalon_lib", "work")
    assert hdl_entity.requires("package", "uart_if_mem_pkg", "work")
    assert hdl_entity.requires("package", "uart_pkg", "work")
    assert len(hdl_entity.required_packages) == 5

    hdl_entity.remove_required_package("work", "uart_pkg")
    assert len(hdl_entity.required_packages) == 4

    hdl_entity.remove_required_package("ieee")
    pprint(hdl_entity.required_packages)
    assert len(hdl_entity.required_packages) == 2

    assert hdl_entity.requires("entity", "simple_pkt_rx", "work")
    assert hdl_entity.requires("entity", "simple_pkt_tx", "work")
    assert hdl_entity.requires("entity", "uart_rx", "work")
    assert hdl_entity.requires("entity", "uart_tx", "work")
    assert hdl_entity.requires("entity", "amm_dual_port_ram", "work")
    assert len(hdl_entity.required_entities) == 5

    assert hdl_entity.provides_entity("uart_if_mem")
    assert len(hdl_entity.provided_entities) == 1

def test_verilog_parsing():
    vlog_file=HdlFile(os.path.join(here, "..", "test", "sv_test.sv"))

    obj = vlog_file.objects
    const = vlog_file.constants
    proc = vlog_file.procedures
    func = vlog_file.functions
    entity = vlog_file.entities

    pprint(obj)

    assert len(const) == 0
    assert len(proc) == 0
    assert len(func) == 0
    assert len(entity) == 1

    assert len(vlog_file.required_packages) == 2
    assert vlog_file.requires("package", "imported_package1", "work")
    assert vlog_file.requires("package", "imported_package2", "work")

if __name__ == "__main__":
    test_hdl_file()
    test_verilog_parsing()