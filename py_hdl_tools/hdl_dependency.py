import os
from pprint import pprint
from .hdl_file import *

here = os.path.abspath(os.path.dirname(__file__))


def _print_entity_or_component(object):
    o = object
    print('{} "{}":'.format(o.kind, o.name))

    print('  Generics:')
    for p in o.generics:
        print('\t{:20}{:8} {}'.format(p.name, p.mode, p.data_type))

    print('  Ports:')
    for p in o.ports:
        print('\t{:20}{:8} {}'.format(p.name, p.mode, p.data_type))


def get_best_guess_for_search_order(needle, directories):
    """
    Return the best guess for the search order to find the given needle
    in a list of directories or files.
    """
    split_dirs = []
    for dir in directories:
        split_dirs += [os.path.split(dir)]

    sorted_dirs = sorted(
        split_dirs,
        key=lambda x: difflib.SequenceMatcher(None, x[1], needle).ratio(),
        reverse=True)

    full_path = []
    for dir in sorted_dirs:
        full_path += [os.path.join(dir[0], dir[1])]
    return full_path


def check_search(haystack):
    assert os.path.split(get_best_guess_for_search_order(
        "uart_if_mem", haystack)[0])[1] == "uart_if_mem"
    assert os.path.split(get_best_guess_for_search_order(
        "uart_tiny", haystack)[0])[1] == "uart_tx_tiny"
    assert os.path.split(get_best_guess_for_search_order(
        "uart_pkg", haystack)[0])[1] == "uart_pack"


def test_get_best_guess_for_search_order():
    haystack = ["uart_tx", "uart_tx_tiny", "uart_rx",
                "uart_if_mem_pkg", "uart_pack", "uart_if_mem"]
    check_search(haystack)

    haystack_with_path = []
    for h in haystack:
        haystack_with_path += [os.path.join("some", "path", h)]

    check_search(haystack_with_path)


def filter_by_filetype(files, file_ext):
    ret = []
    for f in files:
        if os.path.splitext(f)[1].lower() in file_ext:
            ret += [f]
    return ret


class HdlDependency:
    """ HDL file dependency management """

    def __init__(self, repositories=["."], excluded_dir_names=[]):
        self.repositories = repositories
        self.hdl_file_cache = {}
        self.missing_dep = []
        self.excluded_dir_names = excluded_dir_names

        self.ignored_dependencies = [
            "ieee",
            "std",
            "altera",
            "altera_mf",
            "altera_lnsim",
            "lpm",
            "cycloneive"
        ]

        # File list with proper compilation order
        self.filelist = []

    def _add_to_hdl_file_cache(self, filename, hdl_info):
        assert filename not in self.hdl_file_cache.keys()
        self.hdl_file_cache[filename] = hdl_info

    def _get_dep_from_cache(self, dep_type, name, library="work"):
        """ Get dependency from cache or return None """
        assert dep_type in ("entity", "package")

        for _, hdl_info in self.hdl_file_cache.items():
            if hdl_info.provides(dep_type, name, library):
                return hdl_info

        return None

    def get_hdl_file(self, filename):
        """ Get the given HDL file and do some caching """
        try:
            hdl_info = self.hdl_file_cache[filename]
        except KeyError:
            hdl_info = None

        if hdl_info == None:
            hdl_info = HdlFile(filename)
            self._add_to_hdl_file_cache(filename, hdl_info)

        return hdl_info

    def _add_to_filelist(self, filename):
        self.resolve_dependencies_of_file(filename)

        if filename not in self.filelist:
            self.filelist.append(filename)

    def add_missing_dep(self, dep_type, name, library="work"):
        key = dep_type + "." + library + "." + name

        if key not in self.missing_dep:
            self.missing_dep += [key]

    def is_missing_dep(self, dep_type, name, library="work"):
        """ Return true if given dependency is missing """
        key = dep_type + "." + library + "." + name
        return key in self.missing_dep

    @property
    def missing_dependencies(self):
        """ Return a list of missing dependencies """
        return self.missing_dep

    def resolve_dependencies_of_file(self, filename):
        """
        Resolve dependencies of the given file and return
        a file list in the correct compilation order. Note that this
        filelist also contains files for entities which were previously
        resolved with this functions.
        """

        hdl_file = self.get_hdl_file(filename)

        for dep in self.ignored_dependencies:
            hdl_file.remove_required_package(dep)

        library = "work"

        # Start to search in the local directory where the file is stored,
        # then search in the repositories
        # TODO: what if the dependency is one directory up?
        dirs = [os.path.dirname(filename)] + self.repositories

        for dep_type in ("package", "entity"):
            for dep in hdl_file.get_required(dep_type):
                fn = self.resolve_dependency(
                    dep_type, dep["name"], library, dirs)

                if not fn:
                    print("  Couldn't find {} '{}' requested by '{}'".format(
                        dep_type, dep["name"], filename))
                    self.add_missing_dep(dep_type, dep["name"], library)
                else:
                    self._add_to_filelist(fn)

        return self.filelist

    def resolve_dependency(self, dep_type, dep_name, library, dirs):
        """Resolve a single dependency"""
        # TODO: what if dir changes?

        # Skip everything if we already detected that it is a missing dependency
        if self.is_missing_dep(dep_type, dep_name, library):
            return None

        # Check if we already have it cached
        hdl_info = self._get_dep_from_cache(dep_type, dep_name, library)
        if hdl_info:
            return hdl_info.filename

        # TODO: library
        return self.find_dependency(dep_type, dep_name, dirs)

    @staticmethod
    def _get_files_and_dirs(dir, file_ext=None, excluded_dir_names=[], verbose=False):
        files = []
        dirs = []

        for f in os.listdir(dir):
            fn = os.path.join(dir, f)
            if os.path.isfile(fn):
                if file_ext != None and os.path.splitext(f)[1].lower() not in file_ext:
                    continue
                files += [f]
            elif os.path.isdir(fn):
                if f not in excluded_dir_names:
                    dirs += [f]
                else:
                    if verbose:
                        print("  Ignore directory '{}'".format(f))

        return (files, dirs)

    def find_dependency(self, dep_type, dep_name, dirs):
        """
        Return the filename of the file which resolves the given dependency.
        """

        assert dep_type in ("package", "entity")

        # Note: don't sort directory here, because here the order may matters
        # as the first is usually "." and the rest are other repositories.
        for dir in dirs:
            files, subdirs = HdlDependency._get_files_and_dirs(
                dir, HdlFile.vhdl_file_ext + HdlFile.vlog_file_ext,
                self.excluded_dir_names)
            # Start searching in the local directory

            # Sort the files by name, because there is a good chance that
            # the package "uart_pkg" is stored in the file "uart_pack.vhd"
            sorted_files = get_best_guess_for_search_order(dep_name, files)

            for file in sorted_files:
                fn = os.path.join(dir, file)

                hdl_obj = self.get_hdl_file(fn)
                if hdl_obj.provides(dep_type, dep_name):
                    print("Found {} '{}' in '{}'".format(
                        dep_type, dep_name, fn))
                    return fn

            # Go through subfolders.
            # Again sort directory names to increase chance for an early hit.
            sorted_dirs = get_best_guess_for_search_order(
                dep_name, subdirs)

            for i, d in enumerate(sorted_dirs):
                sorted_dirs[i] = os.path.join(dir, d)

            fn = self.resolve_dependency(
                dep_type, dep_name, "work", sorted_dirs)
            if fn != None:
                return fn

        return None


def test_hdl_dependency():
    repositories = [os.path.join(here, "..", "VHDL_lib")]
    dep = HdlDependency(repositories)
    top_file = os.path.join(
        repositories[0], "uart_if", "rtl", "uart_if_mem.vhd")
    pprint(dep.get_hdl_file(top_file))

    file_deps = dep.resolve_dependencies_of_file(top_file)
    pprint(file_deps)

    pprint(dep.missing_dependencies)
    assert len(dep.missing_dependencies) == 0


if __name__ == "__main__":
    test_get_best_guess_for_search_order()
    test_hdl_dependency()
