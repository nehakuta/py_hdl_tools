import os
import subprocess
from pprint import pprint
from .hdl_dependency import HdlDependency
from .tcl_tools import *

here = os.path.abspath(os.path.dirname(__file__))


class HdlSim:
    """ HDL Simulator Control """

    def __init__(self, tb_files, repositories=["."], simulator="modelsim",
                 excluded_dir_names=[]):
        # For now we only support modelsim
        assert simulator in ["modelsim", ]
        self.output_dir = "sim_out"
        self.hdl_dep = HdlDependency(repositories, excluded_dir_names)

        # Testbench files to run
        self.tb_files = []
        for f in tb_files:
            self.tb_files += [os.path.abspath(f)]

    @staticmethod
    def _compile_script_header(project_name="sim_project", project_location="."):
        return \
            "if [batch_mode] {\n" \
            "    onerror {quit -f -code 1}\n" \
            "    onbreak {quit -f -code 2}\n" \
            "}\n" \
            "\n" \
            "# Catch error which is thrown if there is already a project file\n" \
            "catch {project close}\n" \
            "project new %s %s\n" % (project_location, project_name)

    @staticmethod
    def _compile_file(fn):
        fn = os.path.abspath(fn)
        return "project addfile %s\n" % fix_filename_for_tcl(fn)

    def get_compile_script(self):
        """ Return the compile script for all testbench files given """
        for tb_file in self.tb_files:
            self.hdl_dep.resolve_dependencies_of_file(tb_file)

        script = HdlSim._compile_script_header()

        # Note: add testbench files to the end!
        all_files = self.hdl_dep.filelist + self.tb_files

        for filename in all_files:
            script += HdlSim._compile_file(filename)

        script += \
            "\n" \
            "# Calculate compile order, also does compiling\n" \
            "project calculateorder\n"

        return script

    @staticmethod
    def _entity_sim_script(entity_name, waves=[]):
        tcl = \
            "puts \"################################################\"\n" \
            "puts \"Simulating '{0}'\"\n" \
            "vsim {0}\n".format(entity_name)

        for group_name, wave_pattern in waves:
            tcl += "add wave -group {} {}\n".format(group_name, wave_pattern)

        tcl += \
            "run -all\n" \
            "puts \"  done!\"\n" \
            "\n"
        return tcl

    def _sim_script_header(compile_script="compile.do"):
        return \
            "# Modelsim simulation script. Run it with 'vsim -c -do simulate.do' for batch mode\n" \
            "# or with 'vsim -novopt -do simulate.do' for UI mode.\n" \
            "if [batch_mode] {\n" \
            "    # Exit automatically on errors and breaks \n" \
            "    # when running in batch mode \n" \
            "    onerror {quit -f -code 3}\n" \
            "    onbreak {quit -f -code 4}\n" \
            "}\n" \
            "do %s\n\n" % fix_filename_for_tcl(
                os.path.basename(compile_script))

    def get_simulation_script(self, waves=[]):
        """ Return the simulation script which will run all test benches """
        script = HdlSim._sim_script_header()

        for tb_file in self.tb_files:
            hdl_file = self.hdl_dep.get_hdl_file(tb_file)
            for entity in hdl_file.provided_entities:
                script += HdlSim._entity_sim_script(entity["name"], waves)

        script += \
            "\n" \
            "# Exit simulator if running in batch mode\n" \
            "if [batch_mode] {\n" \
            "    quit -f -code 0\n" \
            "}\n"

        return script

    def _run_simulator(self, sim_script, with_ui=False):
        if with_ui:
            # Note: -novopt is needed for QuestaSim
            cmd = "vsim -novopt -do \"%s\"" % sim_script
        else:
            cmd = "vsim -c -novopt -do \"%s\"" % sim_script

        subprocess.run(cmd, shell=True, cwd=self.output_dir, check=True)

    def simulate(self, entity_name=None, with_ui=False, waves=[]):
        """
        Simulate the given entity name. If no entity name is given all entites
        which were given to HdlSim() will be simulated.
        """
        assert entity_name == None  # not supported yet

        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)

        if with_ui and len(waves) == 0:
            waves = [("DUT", "/DUT/*"), ("TB", "*")]

        compile_script = self.get_compile_script()
        sim_script = self.get_simulation_script(waves)

        compile_script_fn = os.path.join(self.output_dir, "compile.do")
        sim_script_fn = os.path.join(self.output_dir, "simulate.do")

        open(compile_script_fn, "w").write(compile_script)
        open(sim_script_fn, "w").write(sim_script)

        self._run_simulator("simulate.do", with_ui=with_ui)


def test_hdl_sim():
    repositories = [os.path.join(here, "..", "VHDL_lib")]
    tb_files = [os.path.join(repositories[0], "uart_if",
                             "tb", "uart_if_mem_tb.vhd")]
    tb_files += [os.path.join(repositories[0],
                              "uart_if", "tb", "uart_if_tb.vhd")]

    hdl_sim = HdlSim(tb_files, repositories)
    hdl_sim.simulate()


if __name__ == "__main__":
    test_hdl_sim()
