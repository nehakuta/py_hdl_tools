-------------------------------------------------------------------------------
-- Title      :
-- Project    : 
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2015 OMICRON electronics GmbH
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.fifo_if_pkg.all;
use work.avalon_lib.all;
use work.avalon_lib.all; -- double use to check if it is added only once

package uart_if_mem_pkg is
  constant TXIF_SIZE      : positive := 4;
  constant RXIF_SIZE      : positive := 4;
  constant TXBUF_ADR_BITS : integer  := 8;
  constant RXBUF_ADR_BITS : integer  := 8;

  constant UARTIF_AVS_ADR_BITS : integer := 10;
  constant UARTIF_SIZE         : integer := 2**UARTIF_AVS_ADR_BITS;
  constant UARTIF_MEM_AVS      : mmap_t  := mmap_master(UARTIF_SIZE);

  constant TXBUF : mmap_t := mmap_append(mmap_child(UARTIF_MEM_AVS), 2**TXBUF_ADR_BITS);
  constant RXBUF : mmap_t := mmap_append(TXBUF, 2**RXBUF_ADR_BITS);

  constant TXIF : mmap_t := mmap_append(RXBUF, TXIF_SIZE);
  constant RXIF : mmap_t := mmap_append(TXIF, RXIF_SIZE);

  constant REG_LCR             : mmap_t  := mmap_append(RXIF, 1);
  constant BF_LCR_CLKDIV_START : integer := 0;
  constant BF_LCR_CLKDIV_END   : integer := 12;
  constant BIT_LCR_PARITY_EN   : integer := 14;
  constant BIT_LCR_EVEN_PARITY : integer := 15;
end package uart_if_mem_pkg;
