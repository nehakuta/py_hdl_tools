-------------------------------------------------------------------------------
-- Title      : UART interface Sequencer
-- Project    : 
-------------------------------------------------------------------------------
-- File       : uart_if.vhd
-- Author     : RolNus00
-- Created    : 2014-10-22
-- Standard   : VHDL 2008
-- ----------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyright (c) 2014 OMICRON electronics GmbH
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library work;
use work.avalon_lib.all;
use work.uart_if_mem_pkg.all;
use work.uart_pkg.all;

entity uart_if_mem is
  generic (
    MAX_CFG          : uart_cfg_t := uart_cfg(32, 1, 8, 'E', 1);
    TXBUF_SIZE_BYTES : positive   := 256;  -- TODO implement
    RXBUF_SIZE_BYTES : positive   := 256;  -- TODO implement    
    RX_FILTER_BITS   : positive   := 2;
    AVS_DATA_BITS    : positive   := 32);
  port (
    clk : in std_logic;
    rst : in std_logic;

    uart_rx_i    : in  std_logic;
    uart_tx_o    : out std_logic;
    uart_tx_en_o : out std_logic;

    -- Avalon slave for control and status register
    avs_m2s : in  amm_m2s_t;
    avs_s2m : out amm_s2m_t;

    irg_o : out std_logic);
end entity;

architecture rtl of uart_if_mem is
  constant UART_DATA_BITS : positive := MAX_CFG.DATA_BITS;
  constant AST_RX_CFG : ast_cfg_t := make_ast_cfg(
    DATA_BITS  => UART_DATA_BITS,
    ERR_BITS   => UARTRX_NUM_ERR,
    CH_BITS    => 0,
    EMPTY_BITS => 1);

  constant AST_TX_CFG : ast_cfg_t := make_ast_cfg(
    DATA_BITS  => UART_DATA_BITS,
    ERR_BITS   => 0,
    CH_BITS    => 0,
    EMPTY_BITS => 1);

  -----------------------------------------------------------------------------
  -- TX path
  signal txif_2_txuart_o2i : ast_o2i_t;
  signal txif_2_txuart_i2o : ast_i2o_t;

  signal txif_avs_m2s : amm_m2s_t;
  signal txif_avs_s2m : amm_s2m_t;
  signal txif_avm_m2s : amm_m2s_t;
  signal txif_avm_s2m : amm_s2m_t;
  signal txbuf_m2s    : amm_m2s_t;
  signal txbuf_s2m    : amm_s2m_t;

  -----------------------------------------------------------------------------
  -- RX path
  signal rxuart_2_rxif_o2i : ast_o2i_t;
  signal rxuart_2_rxif_i2o : ast_i2o_t;

  signal rxif_avs_m2s : amm_m2s_t;
  signal rxif_avs_s2m : amm_s2m_t;
  signal rxif_avm_m2s : amm_m2s_t;
  signal rxif_avm_s2m : amm_s2m_t;
  signal rxbuf_m2s    : amm_m2s_t;
  signal rxbuf_s2m    : amm_s2m_t;

  signal cfg : uart_cfg_t;
begin
  irg_o <= '0';                         -- TODO

  txif_inst : entity work.simple_pkt_tx
    generic map (
      AST_DATA_BITS => AST_TX_CFG.DATA_BITS,
      AVS_SIZE_UNIT => 8,
      AVM_ADR_BITS  => TXBUF_ADR_BITS,
      AVS_DATA_BITS => AVS_DATA_BITS)
    port map (
      clk     => clk,
      rst     => rst,
      avs_m2s => txif_avs_m2s,
      avs_s2m => txif_avs_s2m,
      avm_m2s => txif_avm_m2s,
      avm_s2m => txif_avm_s2m,
      aso_o2i => txif_2_txuart_o2i,
      aso_i2o => txif_2_txuart_i2o);

  uarttx_inst : entity work.uart_tx
    generic map (
      MAX_CFG => MAX_CFG)
    port map (
      clk => clk,
      rst => rst,

      asi_o2i => txif_2_txuart_o2i,
      asi_i2o => txif_2_txuart_i2o,

      cfg => cfg,

      uart_tx_en_o => uart_tx_en_o,
      uart_tx_o    => uart_tx_o);

  txbuf_inst : entity work.amm_dual_port_ram
    generic map (
      DATA_BITS => AVS_DATA_BITS,
      NUM_WORDS => 2**TXBUF_ADR_BITS,
      BYTE_SIZE => 8)
    port map (
      clk      => clk,
      rst      => rst,
      avs0_m2s => txif_avm_m2s,
      avs0_s2m => txif_avm_s2m,
      avs1_m2s => txbuf_m2s,
      avs1_s2m => txbuf_s2m);

  uartrx_inst : entity work.uart_rx
    generic map (
      RX_FILTER_BITS => RX_FILTER_BITS,
      MAX_CFG        => MAX_CFG)
    port map (
      clk => clk,
      rst => rst,

      aso_o2i => rxuart_2_rxif_o2i,
      aso_i2o => rxuart_2_rxif_i2o,

      cfg => cfg,

      uart_rx_i => uart_rx_i);

  rxif_inst : entity work.simple_pkt_rx
    generic map (
      AST_DATA_BITS => AST_RX_CFG.DATA_BITS,
      AVS_SIZE_UNIT => 8,
      AVM_ADR_BITS  => RXBUF_ADR_BITS,
      AVS_DATA_BITS => AVS_DATA_BITS)
    port map (
      clk     => clk,
      rst     => rst,
      avs_m2s => rxif_avs_m2s,
      avs_s2m => rxif_avs_s2m,
      avm_m2s => rxif_avm_m2s,
      avm_s2m => rxif_avm_s2m,
      asi_o2i => rxuart_2_rxif_o2i,
      asi_i2o => rxuart_2_rxif_i2o);

  rxbuf_inst : entity work.amm_dual_port_ram
    generic map (
      DATA_BITS => AVS_DATA_BITS,
      NUM_WORDS => 2**RXBUF_ADR_BITS,
      BYTE_SIZE => 8)
    port map (
      clk      => clk,
      rst      => rst,
      avs0_m2s => rxif_avm_m2s,
      avs0_s2m => rxif_avm_s2m,
      avs1_m2s => rxbuf_m2s,
      avs1_s2m => rxbuf_s2m);

  sync_proc : process (all) is
  begin
    if rst = '1' then
      cfg             <= MAX_CFG;
      cfg.parity_bits <= 0;
      cfg.clk_div     <= 4;
    elsif rising_edge(clk) then
      -- Line Control Register (LCR)
      if avs_m2s.write = '1' then
        if amm_is_mapped(avs_m2s, REG_LCR.base) then
          cfg.parity_bits <= 0;
          if avs_m2s.writedata(BIT_LCR_PARITY_EN) = '1' then
            cfg.parity_bits <= 1;
          end if;

          cfg.even_parity <= false;
          if avs_m2s.writedata(BIT_LCR_EVEN_PARITY) = '1' then
            cfg.even_parity <= true;
          end if;

          cfg.clk_div <= slv2u(avs_m2s.writedata(BF_LCR_CLKDIV_END downto BF_LCR_CLKDIV_START));
        end if;
      end if;

    end if;
  end process;

  comb_proc : process (all) is
  begin
    avs_s2m      <= adr_not_mapped(avs_m2s);
    rxif_avs_m2s <= reset_record;
    txif_avs_m2s <= reset_record;
    txbuf_m2s    <= reset_record;
    rxbuf_m2s    <= reset_record;

    avs_map(avs_m2s, avs_s2m, txif_avs_m2s, txif_avs_s2m, TXIF);
    avs_map(avs_m2s, avs_s2m, rxif_avs_m2s, rxif_avs_s2m, RXIF);
    avs_map(avs_m2s, avs_s2m, txbuf_m2s, txbuf_s2m, TXBUF);
    avs_map(avs_m2s, avs_s2m, rxbuf_m2s, rxbuf_s2m, RXBUF);

    if avs_m2s.read = '1' then
	  if amm_is_mapped(avs_m2s, REG_LCR.base) then
        amm_read_resp(avs_s2m, AMM_DONT_CARE);

        avs_s2m.readdata(BIT_LCR_PARITY_EN) <= '0';
        if cfg.parity_bits = 1 then
          avs_s2m.readdata(BIT_LCR_PARITY_EN) <= '1';
        end if;

        avs_s2m.readdata(BIT_LCR_EVEN_PARITY) <= '0';
        if cfg.even_parity then
          avs_s2m.readdata(BIT_LCR_EVEN_PARITY) <= '1';
        end if;

        avs_s2m.readdata(BF_LCR_CLKDIV_END downto BF_LCR_CLKDIV_START) <=
          u2slv(cfg.clk_div, BF_LCR_CLKDIV_END + 1);
      end if;
    end if;
  end process;
end architecture;
