"""A setuptools based setup module.
See:
https://packaging.python.org/en/latest/distributing.html
https://github.com/pypa/sampleproject
"""

# Always prefer setuptools over distutils
from setuptools import setup, find_packages
# To use a consistent encoding
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'readme.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='py_hdl_tools',
    version='0.0.1',
    description='Various python tools for working with VHDL',
    long_description=long_description,
    platforms=['Any'],
    url='https://grumpy.omicron.at/hw/py_hdl_tools',
    author='Roland Nussbaumer',
    author_email='roland.nussbaumer@omicronenergy.at',
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 4 - Beta',
        'Operating System :: OS Independent',
        'Intended Audience :: Developers',
        'Topic :: Scientific/Engineering :: Electronic Design Automation (EDA)',

        # Pick your license as you wish (should match "license" above)
        'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 3.5',
    ],
    keywords='VHDL',
    packages=['py_hdl_tools'],
    install_requires=['hdlparse>=1.0.7'],

    # To provide executable scripts, use entry points in preference to the
    # "scripts" keyword. Entry points provide cross-platform support and allow
    # pip to create the appropriate form of executable for the target platform.
    entry_points={
        'console_scripts': [
            'sim_module=sim_module:main',
            'show_hdl_file=show_hdl_file:show_hdl_file',
            'add_context_menu=add_context_menu:add_sim_module_to_context_menu'
        ],
    },
)
